This is a [Next.js](https://nextjs.org/) project. Deployed automatically to [https://awesome-bc.pages.dev](https://awesome-bc.pages.dev)

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Links are defined in `src/data/links.json`

Cheats are defined in `src/data/cheats.json`

The deployment to Cloudflare is done using static export, so any react code needs to be client-side.

## Preview Merge Requests

New Merge requests are automatically deployed to `<branchname>.awesome-bc.pages.dev`, allowing you to review your changes in the environment it'll be deployed to.
