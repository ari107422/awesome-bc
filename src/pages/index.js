import Head from 'next/head'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import ExternalLink from '@/components/ExternalLink'
import Links from '@/data/links.json'


const inter = Inter({ subsets: ['latin'] })

const Title = "Awesome Bondage Club";
const Description = "A list of addons for Bondage Club";
const Url = "https://awesome-bc.pages.dev/";

const [toc] = Links.reduce(
  (acc, category) => {
    acc[0].push(category.title);
    return acc;
  },
  [[], []]
);

export default function Home() {
  return (
    <>
      <Head>
        <title>{Title}</title>
        <meta name="description" content={Description} />
        <meta property="og:title" content={Title} />
        <meta property="og:description" content={Description} />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={Url} />
        <link rel="icon" type="image/png" href="/AwesomeBCLogo.png" />
      </Head>
      <main className={`${styles.main} ${inter.className}`}>
        <h1 className={styles.title}>Awesome Bondage Club</h1>
        {toc.length > 0 && (
          <nav className={styles.toc}>
            <ul>
              {toc.map((title, index) => (
                <li key={index}>
                  <a href={`#${title}`}>{title}</a>
                </li>
              ))}
            </ul>
          </nav>
        )}
        {Links.map((category, index) => (
          <div key={index} className={styles.category}>
            <h2 id={category.title}>{category.title}</h2>
            <h3 className={styles.description}>{category.description}</h3>
            {category.links.map((link, index) => (
              <ExternalLink
                key={index}
                mainLink={link.mainLink}
                sourceLink={link.sourceLink}
                title={link.title}
                className={styles.link}
              >
                {link.description}
              </ExternalLink>
            ))}
          </div>
        ))}
      </main>
    </>
  )
}
