import "@/styles/globals.css";
import Layout from "@/components/Layout";
import Head from "next/head";
import posthog from "posthog-js";
import { PostHogProvider } from "posthog-js/react";

const NEXT_PUBLIC_POSTHOG_KEY =
  "phc_BFGNXFE2ezqZAqqorecv1agbx8IUEgSUyQO5Hxv8qnI";
const NEXT_PUBLIC_POSTHOG_HOST = "https://eu.i.posthog.com";

if (typeof window !== "undefined") {
  // checks that we are client-side
  posthog.init(NEXT_PUBLIC_POSTHOG_KEY, {
    api_host: NEXT_PUBLIC_POSTHOG_HOST,
    person_profiles: "always", // or 'always' to create profiles for anonymous users as well
    loaded: (posthog) => {
      if (process.env.NODE_ENV === "development") posthog.debug(); // debug mode in development
    },
  });
}

export default function App({ Component, pageProps }) {
  return (
    <>
      <PostHogProvider instance={posthog}>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </PostHogProvider>
    </>
  );
}
