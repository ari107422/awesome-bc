import Head from 'next/head'
import { Inter } from 'next/font/google'
import styles from '@/styles/Cheats.module.css'
import hljs from "highlight.js";
import "highlight.js/styles/dark.css";
import { useEffect } from 'react';
import Cheats from '@/data/cheats.json'
import CopyButton from '@/components/CopyButton';


const inter = Inter({ subsets: ['latin'] })

const Title = "Cheats";
const Description = "A list of cheats for Bondage Club";
const Url = "https://awesome-bc.pages.dev/cheats";

export default function Home() {
  useEffect(() => {
    hljs.highlightAll();

  }, []);

  const [toc] = Cheats.reduce(
    (acc, category) => {
      acc[0].push(category.title);
      return acc;
    },
    [[], []]
  );

  return (
    <>
      <Head>
        <title>{`Awesome Bondage Club – ${Title}`}</title>
        <meta name="description" content={Description} />
        <meta property="og:title" content={`Awesome Bondage Club – ${Title}`} />
        <meta property="og:description" content={Description} />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={Url} />
        <link rel="icon" type="image/png" href="/AwesomeBCLogo.png" />
      </Head>
      <main className={`${styles.main} ${inter.className}`}>
        <h1>{Title}</h1>
        {toc.length > 0 && (
          <nav className={styles.toc}>
            <ul>
              {toc.map((title, index) => (
                <li key={index}>
                  <a href={`#${title}`}>{title}</a>
                </li>
              ))}
            </ul>
          </nav>
        )}
        {Cheats.map((category, index) => (
          <div key={index} className={styles.category}>
            <h2 id={category.title}>{category.title}</h2>
            {category.cheats.map((cheat, index) => (
              <div key={index} className={styles.cheat}>
                <h3>{cheat.title}</h3>
                <pre>
                  <code className="language-javascript">{cheat.code}</code>
                </pre>
                <CopyButton text={cheat.code} />
              </div>
            ))}
          </div>
        ))}
      </main>
    </>
  )
}
