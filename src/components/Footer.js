import styles from '@/styles/components/Footer.module.css';
import { Inter } from 'next/font/google';


const inter = Inter({ subsets: ['latin'] });

export default function Footer() {
  return (
    <footer className={`${styles.footer} ${inter.className}`}>
      Created by Ariana Skydancer(107422) <a href="https://gitlab.com/ari107422/awesome-bc">Source</a>
    </footer>
  );
}
