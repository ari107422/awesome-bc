import styles from '@/styles/components/ExternalLink.module.css'
import React from 'react';
import PropTypes from 'prop-types';

const ExternalLink = ({ mainLink, sourceLink, title, className, children }) => {
  return (
    <div className={className}>
      <a href={mainLink} target="_blank" rel="noopener" className={styles.link}>{title}</a>
      &nbsp;
      {sourceLink && <a href={sourceLink} target="_blank" rel="noopener" className={styles.source}>Source</a>}
      &#8287;&#8202;
      {children}
    </div>
  );
};

ExternalLink.propTypes = {
  mainLink: PropTypes.string.isRequired,
  sourceLink: PropTypes.string,
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
};

export default ExternalLink;
