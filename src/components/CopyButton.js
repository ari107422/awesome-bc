import { useState } from 'react'
import styles from '@/styles/components/CopyButton.module.css'

export default function CopyButton({ text }) {
  const [buttonText, setButtonText] = useState("Copy");

  const copy = () => {
    navigator.clipboard.writeText(text);
    setButtonText("Copied!");
    setTimeout(() => {
      setButtonText("Copy");
    }, 2500);
  }

  return (
    <button onClick={copy} className={styles.button}>{buttonText}</button>
  )
}
