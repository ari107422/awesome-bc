import ActiveLink from "@/components/ActiveLink";
import { Inter } from "next/font/google";
import styles from "@/styles/components/Nav.module.css";


const inter = Inter({ subsets: ["latin"] });

export default function Nav() {
  return (
    <nav className={`${styles.Nav} ${inter.className}`}>
      <ActiveLink
        activeClassName={styles.Active}
        className={styles.MenuLink}
        href="/">
        Home
      </ActiveLink>
      <ActiveLink
        activeClassName={styles.Active}
        className={styles.MenuLink}
        href="/cheats">
        Cheats
      </ActiveLink>
    </nav>
  );
}
